﻿using UnityEngine;

/// <summary>
/// Tracks if it is off screen by checking if it has exited the resit area.
/// </summary>
public class RoadSegment : MonoBehaviour {

	private bool isOffScreen_ = false;

	/// <summary>
	/// Sets it to offscreen if it has exited the ResetArea.
	/// Simple work around as has various child renderers so can't just use frustrum culling checks.
	/// </summary>
	/// <param name="other"></param>
	private void OnTriggerExit(Collider other) {	
		if(other.gameObject.tag == "ResetArea") {
			isOffScreen_ = true;
		}
	}
	
	/// <summary>
	/// Whether it is consider out of view of the camera and can be reset.
	/// </summary>
	/// <returns> True if it is off screen. </returns>
	public bool IsOffScreen() {
		return isOffScreen_;
	}

	/// <summary>
	/// Declares it as being onscreen.
	/// Does not actually move it to be onscreen itself, this be done by the calling function.
	/// </summary>
	public void SetOnScreen() {
		isOffScreen_ = false;
	}
}
