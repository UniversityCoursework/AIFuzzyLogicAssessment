﻿using UnityEngine;

/// <summary>
/// Handles events to do with ui panels opening or closing.
/// </summary>
public class EventOpenPanel : MonoBehaviour {

	public delegate void EventHandlerPauseMenu();
	/// <summary>
	/// Event to listen to for when a Ui panel opens.
	/// </summary>
	public static event EventHandlerPauseMenu OnOpenPanel;

	/// <summary>
	/// Event to listen to for when a Ui panel closes.
	/// </summary>
	public static event EventHandlerPauseMenu OnClosePanel;

	/// <summary>
	/// Should be called when a Ui panel opens.
	/// </summary>
	public static void OpenPanel() {
		// notify all listeners to event.
		if (OnOpenPanel != null) {
			OnOpenPanel();
		}
	}

	/// <summary>
	/// Should be called when a Ui Panel Closes.
	/// </summary>
	public static void ClosePanel() {
		// notify all listeners to event.
		if (OnClosePanel != null) {
			OnClosePanel();
		}
	}
}
