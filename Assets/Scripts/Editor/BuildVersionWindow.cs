﻿using UnityEngine;
using UnityEditor;
using System;
using UnityEditor.Callbacks;

/// <summary>
/// The Version Number handling Window, allows manually changing of major/minor/etc.
/// 
/// Used to display Version Information and allow it to be in an easily human readibly format.
/// </summary>
public class BuildVersionWindow : EditorWindow {

	private static string currentVersion_;

	private static int major_;

	private static string versionName_;

	private static int build_;

	/// <summary>
	/// Automatically increment the build number for each unity build.
	/// NOTE: Script expects X.versionName_name.X as bundleVersion.
	/// </summary>
	[PostProcessBuildAttribute(0)]
	static void OnPostprocessBuild(BuildTarget buildTarget, string path) {

		string currentVersion = PlayerSettings.bundleVersion;

		try {
			major_ = Convert.ToInt32(currentVersion.Split('.')[0]);
			
			versionName_ = currentVersion.Split('.')[1];
			build_ = Convert.ToInt32(currentVersion.Split('.')[2]) + 1;

			UpdateBuild();
		}
		catch (Exception e) {
			Debug.Log("Increment Build Version Failed:" + e);
		}
	}

	/// <summary>
	/// Update the stored build version with current major,minor,branch,build.
	/// </summary>
	static void UpdateBuild() {
		PlayerSettings.bundleVersion = major_ + "." + versionName_ + "." + build_;

		PlayerSettings.iOS.buildNumber = "" + build_ + "";
		PlayerSettings.Android.bundleVersionCode = build_;

		currentVersion_ = PlayerSettings.bundleVersion;
	}

	/// <summary>
	/// Expose the Build version Manager in the editor.
	/// Simplifies the proccess and ensures everything is on the same style.
	/// </summary>
	[MenuItem("BuildVersion/Manage Version Number")]
	public static void ShowWindow() {
		currentVersion_ = PlayerSettings.bundleVersion;
		major_ = Convert.ToInt32(currentVersion_.Split('.')[0]);
		versionName_ = currentVersion_.Split('.')[1];
		build_ = Convert.ToInt32(currentVersion_.Split('.')[2]);

		GetWindow(typeof(BuildVersionWindow),true);
	}

	/// <summary>
	/// Renders a custom editor window to expose certain settings.
	/// </summary>
	void OnGUI() {
		titleContent.text = "Build Version";
		GUILayout.Label("Current Version: " + currentVersion_, EditorStyles.boldLabel);

		GUILayout.BeginHorizontal();
		GUILayout.Label("Major Number " + major_.ToString());
		if (GUILayout.Button("▲", GUILayout.Width(20f))) {
			major_++;
			UpdateBuild();
		}
		if (GUILayout.Button("▼", GUILayout.Width(20f))) {
			major_--;
			UpdateBuild();
		}
		GUILayout.EndHorizontal();
		
		versionName_ = EditorGUILayout.TextField("Branch Name ", versionName_);


		if (GUILayout.Button("Change Version Branch Name")) {
			build_ = 0;
			UpdateBuild();
		}
	}
}
