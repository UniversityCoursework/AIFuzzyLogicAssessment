﻿using UnityEngine;

/// <summary>
/// Updates the position of the road by swiping left or right on the screen.	
/// </summary>
public class InputController : MonoBehaviour {
	[SerializeField]
	RoadSegmentController road =null;

	[SerializeField]
	float maxRoadOffset = 5.0f;

	[SerializeField]
	[Tooltip("Multiplier for how fast the road is moved.")]
	float moveSpeed = 20.0f;

	private Vector3 distanceFingerTravelled_;

	private Vector3 previousTouchPosition_;

	private bool wasTouching_ = false;

	/// <summary>
	/// Updates the position of the road by swiping left or right on the screen.	
	/// </summary>
	private void Update() {
		if (IsFirstTouch()) {
			previousTouchPosition_ = Input.mousePosition;
			wasTouching_ = true;
		}		
		if (IsTouching()) {			
			// Update the distance the finger has moved since last press.
			distanceFingerTravelled_ = Input.mousePosition - previousTouchPosition_;
			MoveRoad();
		}

		if (OnFinishTouching()) {
			wasTouching_ = false;
		}
	}

	/// <summary>
	/// Detects the first new touch on the screen.
	/// </summary>
	/// <returns> True if a new touch. </returns>
	private bool IsFirstTouch() {
		return Input.GetMouseButtonDown(0) && !wasTouching_;
	}
	/// <summary>
	/// Detects when if there is a finger touching the screen whilst the game is running.
	/// </summary>
	/// <returns> True if touching the screen and the game is not paused. </returns>
	private bool IsTouching() {
		return Input.GetMouseButton(0) && Time.timeScale !=0;
	}

	/// <summary>
	/// Detects if touching the screen has finished this frame.
	/// </summary>
	/// <returns> True if was previously touching and no longer is. </returns>
	private bool OnFinishTouching() {
		return !IsTouching() && wasTouching_;
	}

	/// <summary>
	/// Moves the road mesh based on current finger position compared to previous finger position.
	/// Uses the amount a finger has moved and the displays width to calculate a rate of change, 
	/// and then moves the road clamped between some maximum values.
	/// </summary>
	private void MoveRoad() {
		// calculate a rate of change based on amount finger has moved and the displays width.
		float xChange = distanceFingerTravelled_.x / Screen.width;

		// calculate the new position and clamp it.
		float newXPosition = road.transform.position.x + (moveSpeed * xChange);
		if(newXPosition > maxRoadOffset) {
			newXPosition = maxRoadOffset;
		}

		if (newXPosition < -maxRoadOffset) {
			newXPosition = -maxRoadOffset;
		}

		// update the roads position.
		road.transform.position = new Vector3(newXPosition,
			road.transform.position.y ,
			road.transform.position.z);

		// Update the prevous touch position, so know where finger was even with small movements.
		previousTouchPosition_ = Input.mousePosition;		
	}

}