﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Globalization;
using System.Text;

public class TestData {
	public double distanceToLine =0;
	public double changeInDistanceToLine=0;
	public double result=0;
}

/// <summary>
/// Simple File Parser that reads a set of test data from testdata.csv.
/// This then generates a set of results using the <see cref="FuzzyCarRules.GetCorrectiveSteering"/> Function.
/// </summary>
public class FileParser : MonoBehaviour {
	private List<TestData> testDataFromFile = new List<TestData>();
	private StringBuilder testDataResultsOutput = new StringBuilder();
	private string seperator = ",";

	/// <summary>
	/// Whilst in the editor it will output the test data's results to file the first time it runs.
	/// </summary>
	private void Start() {
#if UNITY_EDITOR
		BuildTestDataList("testdata.csv");
		CalculateResults();
		SaveResults();
#endif
	}

	/// <summary>
	/// Parses Testdata CSV into easily usable list of <see cref="TestData"/>
	/// </summary>
	/// <param name="filename">File name of CSV file to build <see cref="TestData"/> list from (Assumes , separation)</param>
	private void BuildTestDataList(string filename) {

		List<List<string>> filedata;

		string fileToOpen = Path.Combine(Application.dataPath, filename);
		Debug.Log("fileToOpen: " + fileToOpen);

		// Use stream reader to split all data in to a 2d list [LINE NO][ELEMENT] ...(element = x, y, z, scale ,texture type)
		StreamReader sr = File.OpenText(fileToOpen);
		filedata = sr.ReadToEnd().Split('\n').Select(s => s.Split(',').ToList()).ToList();
		sr.Close();

		int paramCount = 2;
		foreach (List<string> str in filedata) {
			if (str.Count() > paramCount) {
				Debug.Log("Invalid File Data:" + fileToOpen);
				continue;
			}
			// newPickUp to be added to the list
			TestData newTestData = new TestData();

			double dist, change;
			// Parse x,y,z values from string
			if (double.TryParse(str[0], NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat, out dist) &&
			double.TryParse(str[1], NumberStyles.Number, CultureInfo.InvariantCulture.NumberFormat, out change)) {
				newTestData.distanceToLine = dist;
				newTestData.changeInDistanceToLine = change;
				testDataFromFile.Add(newTestData);				
				continue;
			}
		}
	}

	/// <summary>
	/// Uses list of <see cref="TestData"/> to calculate result values.
	/// </summary>
	private void CalculateResults() {
		foreach (var item in testDataFromFile) {
			item.result = FuzzyCarRules.GetCorrectiveSteering(item.distanceToLine, item.changeInDistanceToLine);

			testDataResultsOutput.AppendLine(item.distanceToLine + seperator + item.changeInDistanceToLine + seperator + item.result);
		}
	}

	/// <summary>
	/// Saves the currently stored results of <see cref="testDataResultsOutput"/>  to "results.csv".
	/// </summary>
	private void SaveResults() {
		string fileToSave = Path.Combine(Application.dataPath, "results.csv");
		File.Create(fileToSave).Dispose();

		File.AppendAllText(fileToSave, testDataResultsOutput.ToString());
	}
}
