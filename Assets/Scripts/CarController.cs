﻿using UnityEngine;

/// <summary>
/// Controls the car within the scene, using fuzzy logic to control the cars steering.
/// Allows the initial values of the car to be set in the editor, or directly set from the <see cref="FuzzyAiCalculator"/> in the settings panel.
/// </summary>
public class CarController : MonoBehaviour {
	
	[SerializeField]
	float distanceNormalizer = 5.0f;

	[Range(1,100)]
	[SerializeField]
	float speed = 10.0f;

	[Range(1,90)]
	[SerializeField]
	float maxTurningAngle = 10.0f;

	[Range(-1,1)]
	[SerializeField]
	float initialDirection = 1.0f;

	[SerializeField]
	float initialDistanceToCentre = 1.0f;

	[SerializeField]
	Transform roadCentre = null;

	private double currentSteering_ = 0.0f;

	private double currentDistanceToCentre_ = 0.0f;

	private double correctiveSteering_ = 0.0f;

	private Vector3 velocity_;

	private bool isUsingCorrectiveSteering_ = false;

	/// <summary>
	/// Sets the intial values for the car.
	/// </summary>
	private void Start() {
		SetInitialValues(initialDistanceToCentre / distanceNormalizer, initialDirection);		
	}

	/// <summary>
	/// Updates the car position and then calculates the <see cref="FuzzyCarRules.GetCorrectiveSteering"/> and applies it to the car.
	/// </summary>
	private void Update() {
		if (Time.timeScale != 0) {
			velocity_ = (speed * Time.deltaTime * transform.forward);
			// update position.
			transform.position += velocity_;

			// calculate distance to centre, clamping to -1 -> 1
			currentDistanceToCentre_ = (transform.position.x - roadCentre.transform.position.x) / distanceNormalizer;
			currentDistanceToCentre_ = MathUtil.Clamp(currentDistanceToCentre_, -1, 1);

			// calculate new correct steering value.
			correctiveSteering_ = FuzzyCarRules.GetCorrectiveSteering(currentDistanceToCentre_, velocity_.x);
			if (isUsingCorrectiveSteering_) {
				// apply to current steering.
				currentSteering_ += correctiveSteering_;
				// clamp steering to physically possible values ( just in case).
				currentSteering_ = MathUtil.Clamp(currentSteering_, -1, 1);
			}
			else {
				currentSteering_ = MathUtil.Clamp(correctiveSteering_, -1, 1);
			}

			transform.rotation = Quaternion.Euler(0, maxTurningAngle * (float)currentSteering_, 0);
		}
	}

	/// <summary>
	/// Updates the position of the car when it is changed in the editor.
	/// </summary>
	private void OnValidate() {
		currentSteering_ = initialDirection;
		currentDistanceToCentre_ = initialDistanceToCentre / distanceNormalizer;

		transform.position = new Vector3((float)(initialDistanceToCentre), 0, transform.position.z);

		// clamp speed sensibly.
		if(speed < 1.0f) {
			speed = 1.0f;
		}
	}
	
	public void ToggleCorrectiveSteering() {
		isUsingCorrectiveSteering_ = !isUsingCorrectiveSteering_;
	}
	public double RateOfChangeToCentre() {
		return velocity_.x;
	}

	public double DistanceToCentre() {
		return currentDistanceToCentre_;
	}

	public double CorrectiveSteering() {
		return correctiveSteering_;
	}

	public float Speed() {
		return speed;
	}

	public float MaxTurningAngle() {
		return maxTurningAngle;
	}

	public void SetSpeed(float newSpeed) {
		speed = newSpeed;
	}

	public void SetMaxTurningAngle(float newMaxAngle) {
		maxTurningAngle = newMaxAngle;
	}

	/// <summary>
	/// Resets the car to match the passed in values.
	/// Repositioning the car and correcting the car's direction.
	/// </summary>	
	/// <param name="distanceToCentre">In Range -1 to 1.  </param>
	/// <param name="direction"> In Range -1 to 1. </param>
	public void SetInitialValues(double distanceToCentre, double rateOfChange) {
		currentSteering_ = rateOfChange;
		currentDistanceToCentre_ = distanceToCentre;
					
		transform.position = new Vector3(roadCentre.transform.position.x + (float)(distanceToCentre * distanceNormalizer), 0, transform.position.z);
		transform.rotation = Quaternion.Euler(0, maxTurningAngle * (float)currentSteering_, 0);
	}

}
