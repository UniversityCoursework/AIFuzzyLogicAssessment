﻿using UnityEngine;

/// <summary>
/// This script will cause the attached game object to follow the car.
/// Only follows on the z-axis, this is used in conjuction with the Camera,
///  and the Reset area, to allow the car to turn without the camera turning.
/// </summary>
public class CameraRig : MonoBehaviour {

	[SerializeField]
	GameObject car = null;
		
	/// <summary>
	/// Follows the car along the z-axis.
	/// </summary>
	private void LateUpdate () {
		transform.position = new Vector3(transform.position.x, transform.position.y, car.transform.position.z);
	}
}
