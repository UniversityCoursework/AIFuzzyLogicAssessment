﻿using UnityEngine;
using FLS;
using FLS.Rules;

/// <summary>
/// Fuzzy Car Steering 
/// </summary>
public class FuzzyCarRules : MonoBehaviour {

	/// <summary>
	/// Using input values and fuzzy logic calculates an additive corrective steering to be use to bring it back to the centre line.
	/// </summary>
	/// <param name="currentDistanceToLine"> In Range -1 to 1. </param>
	/// <param name="currentChangeInDistanceToLine"> In Range -1 to 1. </param>
	/// <returns> Corrective steering value to be added to current steering in Range -1 to 1. </returns>
	public static double GetCorrectiveSteering(double currentDistanceToLine, double currentChangeInDistanceToLine) {

		LinguisticVariable changeInDistance = new LinguisticVariable("ChangeInDistance");
		var largeLeftChange = changeInDistance.MembershipFunctions.AddTriangle("LargeLeft", -1, -1, -0.5);
		var leftChange = changeInDistance.MembershipFunctions.AddTriangle("Left", -1, -0.5, 0);
		var centreChange = changeInDistance.MembershipFunctions.AddTriangle("Centre", -0.5, 0, 0.5);
		var rightChange = changeInDistance.MembershipFunctions.AddTriangle("Right", 0, 0.5, 1);
		var largeRightChange = changeInDistance.MembershipFunctions.AddTriangle("LargeRight", 0.5, 1, 1);

		LinguisticVariable distance = new LinguisticVariable("Distance");
		var largeLeftDistance = distance.MembershipFunctions.AddTriangle("LargeLeft", -1, -1, -0.5);
		var leftDistance = distance.MembershipFunctions.AddTriangle("Left", -1, -0.5, 0);
		var centreDistance = distance.MembershipFunctions.AddTriangle("Centre", -0.5, 0, 0.5);
		var rightDistance = distance.MembershipFunctions.AddTriangle("Right", 0, 0.5, 1);
		var largeRightDistance = distance.MembershipFunctions.AddTriangle("LargeRight", 0.5, 1, 1);

		LinguisticVariable changeInSteering = new LinguisticVariable("Distance");
		var largeLeftSteering = changeInSteering.MembershipFunctions.AddTriangle("LargeLeft", -1, -1, -0.5);
		var leftSteering = changeInSteering.MembershipFunctions.AddTriangle("Left", -1, -0.5, 0);
		var centreSteering = changeInSteering.MembershipFunctions.AddTriangle("Centre", -0.5, 0, 0.5);
		var rightSteering = changeInSteering.MembershipFunctions.AddTriangle("Right", 0, 0.5, 1);
		var largeRightSteering = changeInSteering.MembershipFunctions.AddTriangle("LargeRight", 0.5, 1, 1);


		IFuzzyEngine fuzzyEngine = new FuzzyEngineFactory().Create((FuzzyEngineType.CoG));
		
		FuzzyRule[] rules = new FuzzyRule[25];

		rules[0] = Rule.If(changeInDistance.Is(largeLeftChange).And(distance.Is(largeLeftDistance))).Then(changeInSteering.Is(largeRightSteering));
		rules[1] = Rule.If(changeInDistance.Is(leftChange).And(distance.Is(largeLeftDistance))).Then(changeInSteering.Is(largeRightSteering));
		rules[2] = Rule.If(changeInDistance.Is(centreChange).And(distance.Is(largeLeftDistance))).Then(changeInSteering.Is(rightSteering));
		rules[3] = Rule.If(changeInDistance.Is(rightChange).And(distance.Is(largeLeftDistance))).Then(changeInSteering.Is(rightSteering));
		rules[4] = Rule.If(changeInDistance.Is(largeRightChange).And(distance.Is(largeLeftDistance))).Then(changeInSteering.Is(centreSteering));

		rules[5] = Rule.If(changeInDistance.Is(largeLeftChange).And(distance.Is(leftDistance))).Then(changeInSteering.Is(largeRightSteering));
		rules[6] = Rule.If(changeInDistance.Is(leftChange).And(distance.Is(leftDistance))).Then(changeInSteering.Is(rightSteering));
		rules[7] = Rule.If(changeInDistance.Is(centreChange).And(distance.Is(leftDistance))).Then(changeInSteering.Is(rightSteering));
		rules[8] = Rule.If(changeInDistance.Is(rightChange).And(distance.Is(leftDistance))).Then(changeInSteering.Is(centreSteering));
		rules[9] = Rule.If(changeInDistance.Is(largeRightChange).And(distance.Is(leftDistance))).Then(changeInSteering.Is(leftSteering));

		rules[10] = Rule.If(changeInDistance.Is(largeLeftChange).And(distance.Is(centreDistance))).Then(changeInSteering.Is(rightSteering));
		rules[11] = Rule.If(changeInDistance.Is(leftChange).And(distance.Is(centreDistance))).Then(changeInSteering.Is(rightSteering));
		rules[12] = Rule.If(changeInDistance.Is(centreChange).And(distance.Is(centreDistance))).Then(changeInSteering.Is(centreSteering));
		rules[13] = Rule.If(changeInDistance.Is(rightChange).And(distance.Is(centreDistance))).Then(changeInSteering.Is(leftSteering));
		rules[14] = Rule.If(changeInDistance.Is(largeRightChange).And(distance.Is(centreDistance))).Then(changeInSteering.Is(leftSteering));

		rules[15] = Rule.If(changeInDistance.Is(largeLeftChange).And(distance.Is(rightDistance))).Then(changeInSteering.Is(rightSteering));
		rules[16] = Rule.If(changeInDistance.Is(leftChange).And(distance.Is(rightDistance))).Then(changeInSteering.Is(centreSteering));
		rules[17] = Rule.If(changeInDistance.Is(centreChange).And(distance.Is(rightDistance))).Then(changeInSteering.Is(leftSteering));
		rules[18] = Rule.If(changeInDistance.Is(rightChange).And(distance.Is(rightDistance))).Then(changeInSteering.Is(leftSteering));
		rules[19] = Rule.If(changeInDistance.Is(largeRightChange).And(distance.Is(rightDistance))).Then(changeInSteering.Is(largeLeftSteering));

		rules[20] = Rule.If(changeInDistance.Is(largeLeftChange).And(distance.Is(largeRightDistance))).Then(changeInSteering.Is(centreSteering));
		rules[21] = Rule.If(changeInDistance.Is(leftChange).And(distance.Is(largeRightDistance))).Then(changeInSteering.Is(leftSteering));
		rules[22] = Rule.If(changeInDistance.Is(centreChange).And(distance.Is(largeRightDistance))).Then(changeInSteering.Is(leftSteering));
		rules[23] = Rule.If(changeInDistance.Is(rightChange).And(distance.Is(largeRightDistance))).Then(changeInSteering.Is(largeLeftSteering));
		rules[24] = Rule.If(changeInDistance.Is(largeRightChange).And(distance.Is(largeRightDistance))).Then(changeInSteering.Is(largeLeftSteering));

		fuzzyEngine.Rules.Add(rules);
		
		return fuzzyEngine.Defuzzify(new { changeInDistance = currentChangeInDistanceToLine, distance = currentDistanceToLine });
	
	}
}
