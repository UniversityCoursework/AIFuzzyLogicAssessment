﻿using UnityEngine;

/// <summary>
/// Controls a set of <see cref="RoadSegment"/>'s making an endless road.
/// It does this by reseting segments that are off screen and attaching them to the end.
/// </summary>
public class RoadSegmentController : MonoBehaviour {

	[SerializeField]
	RoadSegment[] roadSegments = new RoadSegment[0];

	[SerializeField]
	RoadSegment lastRoadSegment = null;

	[SerializeField]
	float roadLength = 120;

	private Vector3 moveDirection_ = new Vector3(0,0,-1);

	/// <summary>
	/// Updates positon of all <see cref="RoadSegment"/>'s.	
	/// </summary>
	private void Update() {
		UpdateMovers();
	}

	/// <summary>
	/// Updates <see cref="RoadSegment"/>'s reseting offscreen segments to the end of the set.
	/// </summary>
	private void UpdateMovers() {
		foreach (RoadSegment roadSegment in roadSegments) {
			if (roadSegment.IsOffScreen()) {
				// Move segment to the end of the set.
				roadSegment.transform.localPosition = lastRoadSegment.transform.localPosition + (moveDirection_ * -roadLength);
				roadSegment.SetOnScreen();
				// update last segment
				lastRoadSegment = roadSegment;
			}				
		}
	}

}
