﻿/// <summary>
/// Simple Utilities class that exposes some common math functions.
/// </summary>
public class MathUtil {

	/// <summary>Clamp a value between a min and max range </summary>
	/// <param name="val"> The value to be clamped. </param>
	/// <param name="upper"> The upper range value. </param>
	/// <param name="lower"> The lower range value. </param>
	/// <returns> The value clamped. </returns>	
	public static double Clamp(double val, double min, double max) {
		if (val > max) {
			val = max;
		}
		if (val < min) {
			val = min;
		}
		return val;
	}
}
