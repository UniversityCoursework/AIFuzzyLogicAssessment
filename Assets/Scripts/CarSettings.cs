﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Exposes the speed and max turning angle of the car as sliders.
/// </summary>
public class CarSettings : MonoBehaviour {

	[SerializeField]
	CarController sceneCarController = null;

	[SerializeField]
	Slider maxTurningAngleSlider = null;

	[SerializeField]
	Slider speed = null;

	[SerializeField]
	Toggle correctiveSteering = null;

	/// <summary>
	/// Links up sliders to the car, and adds the listenrs for the sliders changing.
	/// </summary>
	private void OnEnable() {
		maxTurningAngleSlider.onValueChanged.AddListener(delegate { UpdateTurningAngle(); });
		speed.onValueChanged.AddListener(delegate { UpdateSpeed(); });

		maxTurningAngleSlider.value = sceneCarController.MaxTurningAngle();
		speed.value = sceneCarController.Speed();
		correctiveSteering.onValueChanged.AddListener(ToggleCorrectiveSteering);
	}

	/// <summary>
	/// Remove the listeners when it is disabled.
	/// </summary>
	private void OnDisable() {
		maxTurningAngleSlider.onValueChanged.RemoveListener(delegate { UpdateTurningAngle(); });
		speed.onValueChanged.RemoveListener(delegate { UpdateSpeed(); });
		correctiveSteering.onValueChanged.RemoveListener(ToggleCorrectiveSteering);
	}
	
	private void ToggleCorrectiveSteering(bool state) {
		sceneCarController.ToggleCorrectiveSteering();
	}

	/// <summary>
	/// Update the Max Turning Angle of the car.
	/// </summary>
	private void UpdateTurningAngle() {
		sceneCarController.SetMaxTurningAngle(maxTurningAngleSlider.value);
	}

	/// <summary>
	/// Update the speed of the car.
	/// </summary>
	private void UpdateSpeed() {
		sceneCarController.SetSpeed(speed.value);
	}
}
