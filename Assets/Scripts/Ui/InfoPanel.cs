﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Simple script that shows info about how to use the app.
/// Handles closing and openin the panel from the connected buttons.
/// </summary>
public class InfoPanel : MonoBehaviour {
	[SerializeField]
	Button openInfoPanelButton = null;
	[SerializeField]
	Button closeInfoPanelButton = null;

	/// <summary>
	/// Adds listeners so the panel can be opened and closed.
	/// </summary>	
	private void OnEnable() {		
		openInfoPanelButton.onClick.AddListener(OpenInfoPanel);
		closeInfoPanelButton.onClick.AddListener(CloseInfoPanel);
		EventOpenPanel.OpenPanel();
	}

	/// <summary>
	/// Removes listeners.
	/// </summary>
	private void OnDestroy() {
		openInfoPanelButton.onClick.RemoveAllListeners();
		closeInfoPanelButton.onClick.RemoveAllListeners();
	}

	/// <summary>
	/// Opens the info panel, and pauses all gameplay.
	/// </summary>
	public void OpenInfoPanel() {
		Time.timeScale = 0;
		gameObject.SetActive(true);
		EventOpenPanel.OpenPanel();
	}

	/// <summary>
	/// Closes the info panel and restarts all gameplay.
	/// </summary>
	public void CloseInfoPanel() {
		Time.timeScale = 1;
		gameObject.SetActive(false);
		EventOpenPanel.ClosePanel();
	}
}
