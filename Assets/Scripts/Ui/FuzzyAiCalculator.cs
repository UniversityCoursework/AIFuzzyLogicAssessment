﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Calculates inputed resultant fuzzy values and updates the applications 3d representation aswell.
/// </summary>
public class FuzzyAiCalculator : MonoBehaviour {
	[SerializeField]
	CarController carController = null;

	[SerializeField]
	Button calculateButton = null;

	[SerializeField]
	ClampInputDoubleValue clampedDistanceToCentre = null;
	[SerializeField]
	ClampInputDoubleValue clampedRateOfChangeToCentre = null;

	[SerializeField]
	Text outputDistanceToCentreText = null;
	[SerializeField]
	Text outputRateOfChangeToCentreText = null;
	[SerializeField]
	Text outputFuzzySteeringText = null;

	/// <summary>
	/// Sets the text to the initial values.
	/// </summary>
	private void OnEnable() {
	
		if (!calculateButton) {
			calculateButton = GetComponent<Button>();
		}
		calculateButton.onClick.AddListener(CalculateAndSetNewFuzzyValues);
		
		outputDistanceToCentreText.text = carController.DistanceToCentre().ToString();
		outputRateOfChangeToCentreText.text = carController.RateOfChangeToCentre().ToString();
		outputFuzzySteeringText.text = carController.CorrectiveSteering().ToString();
	}

	private void OnDisable() {
		calculateButton.onClick.RemoveListener(CalculateAndSetNewFuzzyValues);
	}

	/// <summary>
	/// When the user presses the calculate button this calculates new fuzzy values and updates the car controller.
	/// </summary>
	private void CalculateAndSetNewFuzzyValues() {
		double distance = clampedDistanceToCentre.ClampedValue();
		double rateOfChangeToCentre = clampedRateOfChangeToCentre.ClampedValue();

		double steering = FuzzyCarRules.GetCorrectiveSteering(distance, rateOfChangeToCentre);

		outputDistanceToCentreText.text = distance.ToString();
		outputRateOfChangeToCentreText.text = rateOfChangeToCentre.ToString();
		outputFuzzySteeringText.text = steering.ToString();

		carController.SetInitialValues(
			clampedDistanceToCentre.ClampedValue(),
			clampedRateOfChangeToCentre.ClampedValue());
	}
}
