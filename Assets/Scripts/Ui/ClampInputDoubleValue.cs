﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Sets input field value to handle decimals and Clamps InputFields values when edited.
/// </summary>
[RequireComponent(typeof(InputField))]
public class ClampInputDoubleValue : MonoBehaviour {

	[SerializeField]
	double minValue = -1.0f;
	[SerializeField]
	double maxValue = 1.0f;

	InputField inputField_;

	double clampedValue_ = 0.0f;

	/// <summary>
	/// Ensures the input field is set to decimal and connected.
	/// </summary>
	private void Start() {
		inputField_ = GetComponent<InputField>();
		inputField_.contentType = InputField.ContentType.DecimalNumber;
	}

	/// <summary>
	/// Adds listener for when values are changed so can be clamped.
	/// </summary>
	private void OnEnable() {
		if (!inputField_) {
			inputField_ = GetComponent<InputField>();
		}
		inputField_.onEndEdit.AddListener(ClampInputValue);
		inputField_.text = clampedValue_.ToString();
	}

	/// <summary>
	/// Removes all listeners
	/// </summary>
	private void OnDisable() {
		inputField_.onEndEdit.RemoveAllListeners();
	}

	/// <summary>
	/// The current ClampedValue  <see cref="clampedValue_"/>.
	/// </summary>
	/// <returns> The current <see cref="clampedValue_"/> </returns>
	public double ClampedValue() {
		return clampedValue_;
	}

	/// <summary>
	/// Parses the text to doubles and clamps it.
	/// Updating the inputfields text to the new clamped value.
	/// </summary>
	/// <param name="inputText"> The input fields current text. (Taken from onEndEdit event) </param>
	private void ClampInputValue(string inputText) {
		double numberToClamp = double.Parse(inputText);
		clampedValue_ = MathUtil.Clamp(numberToClamp, minValue, maxValue);
		inputText = clampedValue_.ToString();
		inputField_.text = inputText;
	}
	
}
