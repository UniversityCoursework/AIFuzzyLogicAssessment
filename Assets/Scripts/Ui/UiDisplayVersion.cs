﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Simply ensures the version number displays correctly in bottom right of screen.
/// </summary>
[RequireComponent(typeof(Canvas))]
public class UiDisplayVersion : MonoBehaviour {

	[SerializeField]
	bool isPersistant =false;

	/// <summary>
	/// Sets the child text component to display the game version number.
	/// Ensures it persists throughout the life of the game.
	/// </summary>
	void Start() {
		if (isPersistant) {
			DontDestroyOnLoad(this);
			gameObject.name = "PersistentVersionDisplay";
		}
		GetComponentInChildren<Text>().text = Application.version;
	}

	/// <summary>
	/// Ensures the value shownn in the Editor view is the current version.
	/// </summary>
	private void OnValidate() {
		GetComponentInChildren<Text>().text = Application.version;
	}
}
