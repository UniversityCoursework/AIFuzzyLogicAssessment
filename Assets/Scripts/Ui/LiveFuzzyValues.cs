﻿using System.Collections;

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Displays live information of the current fuzzy values from the car controller to the screen.
/// </summary>
public class LiveFuzzyValues : MonoBehaviour {
	[SerializeField]
	CarController carController = null;
	[SerializeField]
	Text outputDistanceToCentreText = null;
	[SerializeField]
	Text outputRateOfChangeToCentreText = null;
	[SerializeField]
	Text outputFuzzySteeringText = null;
	
	/// <summary>
	/// Adds listeners to the opening an closing of panels so only visible when in the main screen.
	/// </summary>
	private IEnumerator Start() {
		EventOpenPanel.OnOpenPanel += OnPanelOpen;
		EventOpenPanel.OnClosePanel += OnPanelClose;

		// Fix: odd initialization bug when set active false when starting.
		yield return new WaitForEndOfFrame();
		gameObject.SetActive(false);
	}

	/// <summary>
	/// Removes all listeners.
	/// </summary>
	private void OnDestroy() {
		EventOpenPanel.OnOpenPanel -= OnPanelOpen;
		EventOpenPanel.OnClosePanel -= OnPanelClose;
	}

	/// <summary>
	/// Updates the text on screen.
	/// </summary>
	private void Update() {
		outputDistanceToCentreText.text = string.Format("{0:0.000}", carController.DistanceToCentre());
		outputRateOfChangeToCentreText.text = string.Format("{0:0.000}", carController.RateOfChangeToCentre());
		outputFuzzySteeringText.text = string.Format("{0:0.000}", carController.CorrectiveSteering());
	}

	/// <summary>
	/// Hides the Live Display.
	/// </summary>
	private void OnPanelOpen() {
		gameObject.SetActive(false);
	}
	/// <summary>
	/// Shows the live display.
	/// </summary>
	private void OnPanelClose() {
		gameObject.SetActive(true);
	}

}
