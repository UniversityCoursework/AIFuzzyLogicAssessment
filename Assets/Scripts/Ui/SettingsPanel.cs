﻿using System.Collections;

using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Simple script that shows the settings panel.
/// Handles closing and openin the panel from the connected buttons.
/// </summary>
public class SettingsPanel : MonoBehaviour {
	[SerializeField]
	Button openSettingsPanelButton = null;
	[SerializeField]
	Button closeSettingsPanelButton = null;

	/// <summary>
	/// Ensures the panel is disabled on game start.
	/// </summary>
	private IEnumerator Start() {
		openSettingsPanelButton.onClick.AddListener(OpenSettingsPanel);
		closeSettingsPanelButton.onClick.AddListener(CloseSettingsPanel);
		
		// Fix: odd initialization bug when set active false when starting.
		yield return new WaitForEndOfFrame();
		gameObject.SetActive(false);
	}
	
	/// <summary>
	/// Adds listeners so the panel can be opened and closed.
	/// </summary>
	private void OnEnable() {
		openSettingsPanelButton.onClick.AddListener(OpenSettingsPanel);
		closeSettingsPanelButton.onClick.AddListener(CloseSettingsPanel);
	}

	/// <summary>
	/// Removes listeners.
	/// </summary>
	private void OnDestroy() {
		openSettingsPanelButton.onClick.RemoveAllListeners();
		closeSettingsPanelButton.onClick.RemoveAllListeners();
	}

	/// <summary>
	/// Opens the setting panel, and pauses all gameplay.
	/// </summary>
	public void OpenSettingsPanel() {		
		Time.timeScale = 0;
		gameObject.SetActive(true);
		EventOpenPanel.OpenPanel();
	}

	/// <summary>
	/// Closes the settings panel and restarts all gameplay.
	/// </summary>
	public void CloseSettingsPanel() {		
		Time.timeScale = 1;
		gameObject.SetActive(false);
		EventOpenPanel.ClosePanel();
	}
}
