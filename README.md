# Ai Fuzzy Logic 

Simple Unity application made for artificial intelligence assessment.

Uses Fuzzy Logic to control the steering of a simple car.

## Doxygen

https://universitycoursework.gitlab.io/AIFuzzyLogicAssessment/

## Fuzzy Logic Library

https://github.com/davidgrupp/Fuzzy-Logic-Sharp

https://www.nuget.org/packages/FLS/

## Unity Version
Unity 2017.2.0b2

